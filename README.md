# Projet Blog PHP

Créer un blog en PHP en utilisant le framework Symfony 4.3 et PDO

* Fonctionnalités attendues
    * Créer des articles
    * Consulter la liste des articles
    * Consulter un article spécifique
    * Modifier ou supprimer un article

Use Case :

![Use Case](./uml/UseCaseDiagram1.jpg)

Diagramme de Classes : 

![Diagramme](./uml/blog.png)

Wireframe : 

![homepage](./uml/homepageWF.png)