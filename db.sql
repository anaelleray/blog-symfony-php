CREATE DATABASE blog_php;

USE blog_php;

CREATE TABLE post (
     id INT PRIMARY KEY auto_increment NOT NULL,
     title VARCHAR (64),
     author VARCHAR (64),
     postDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
     content TEXT
     );

     INSERT INTO post (title, author, content) VALUES ("Just a reminder", "Your mind", "you are enough, you
're so much talented and you can be everything you want. Just remember this" );

     INSERT INTO post (title, author, content) VALUES ("itsnotcomingback,itsnevergone", "anarchyintheUK", "In this city, how many
ways to get what you want?" );






