<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;

class PostController extends AbstractController
{
    /**
     * @Route("/posts", name="posts")
     */
    public function index(Request $request)
    {
        $repo = new PostRepository();
        $newPost = null;
        $author = $request->get("author");
        $title = $request->get("title");
        $content = $request->get("content");
  
        if ($author && $title && $content) {
  
           $newPost = new Post($title, $author, new \DateTime, $content);
  
           $repo->add($newPost);

           return $this->redirectToRoute('posts');
        }

        $posts = $repo->findAll();
    
        return $this->render('home.html.twig', [
            'posts' => $posts
        ]);

    } 


    /**
     * @Route ("/one-article/{id}", name="one-article")
     */
    public function onePost(int $id)

    {
        $repo = new PostRepository();
        $post = $repo->findById($id);
        // dump($post);
        return $this->render('showOneArticle.html.twig', [
            'post' => $post
        ]);
    }



    /**
    * @Route("/del/{id}", name="delete_post")
    */
   public function deletePost(int $id)
   {
      $repo = new PostRepository();
      $repo->delete($id);
      return $this->redirectToRoute('posts');
   }


   /**
   * @Route("/update/{id}", name = "update_post")
   */
  public function updatePost(int $id, Request $request)
   {
      $repo = new PostRepository();
      
      $author = $request->get("author");
      $title = $request->get("title");
      $content = $request->get("content");
      
      if ($author && $title && $content) {
         $repo->update($title,$author,$content,$id);
      }
      $postView = $repo->findById($id);
      
      return $this->render('newForm.html.twig', ['postView' => $postView]);

   }


}
